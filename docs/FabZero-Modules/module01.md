# 1. Documentation

## Bonnes pratiques

Durant le premier cours de Fabzero, il a été dit que l'utilisation d'un système de control de version (version control system) est l'outil le plus puissant pour la documentation d'un projet et le partage de ce document avec nos collaborateurs. Parce que ce système fait une sauvegarde automatique des version et modifications faites possédant une option "historique". Grâce à ça, par exemple, durant une collaboration, si un collaborateur efface par mégarde le texte d'un autre, il suffit de juste de cliquer sur historique pour le revoir.

L'outil utilisé pour ce cours est gitlab, un open-source version control sytem

## Linux

Dans le cas où on souhaite modifier nos documents localement et non sur le site web, on peut communiquer ou transférer entre la bureau de notre ordi et le site gitlab via l'invite de commande avec l'Unix shell Bash.

Possédant le système d'exploitation Windows 10, il est déja présent dans mon ordinateur. Pour l'activer,j'ai suivi ce site [Ineat Blog](https://blog.ineat-group.com/2020/02/utiliser-le-terminal-bash-natif-dans-windows-10/) me permettant d'utiliser des commandes Linux.


## Markdown

Gitlab utilise le format Markdown pour la création de texte étant plus simple que HTML. HTML est un format nécessitant de faire attention aux balise de code comparé à markdown qui se concentre sur le texte en utilisant des symboles pour donner des effets à nos mots d'une manière facile et simple et on peut exporter notre texte facilement au format HTML. Pour la liste des symboles aller voir à ce lien [Markdown](https://en.wikipedia.org/wiki/Markdown)

J'ai installer 2 software:

* Pandoc : pour la conversion en format HTML

* Graphicsmagick : pour l'édition d'images


## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```


## Gallery

![](../images/sample-photo.jpg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/jjNgJFemlC4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
