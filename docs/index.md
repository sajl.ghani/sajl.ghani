Hello!

This is an example blog for the FabZero-Experiments Course.

You can edit it on [Gitlab](http://gitlab.fabcloud.org). The software used turns simple text files
written in [Markdown](https://en.wikipedia.org/wiki/Markdown) format, into the site you are navigating.

Each time you change a page using the Gitlab interface the site is rebuilt and all the changes published
in few minutes.

If this is your site, go on and edit this page clicking the Gitlab link on the upper right, changing the text below and deleting this.

No worries, you can't break anything, all the changes you make are saved under [Version Control](https://en.wikipedia.org/wiki/Version_control) using [GIT](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control). This means that you have all the different versions of your page saved and available all the time in the Gitlab interface.

## A propos de moi

Bonjour, Je m'appelle Ghani Sajl (qu'on prononce Sajil mais on l'écrit sans le i).

Je suis étudiant polytech de l'ULB en dernière année de master en Electromecanique,option robotique et construction mécanique. Dans ma page fablab, vous aurez une description des 6 modules présentés du cours de Fabzero experiment et de la progression de mon projet.

J'ai choisi de suivre ce cours car je suis intéresser par les techniques de fabrication numérique et je voudrais approfondir mes compétences et acquérir plus d'expérience.



## Centre d'intérêt et loisirs

Je suis intéresser par les nouvelles technologies et l'informatique donc je vise un emploi qui allie informatique et électronique. J'aime la lecture et les jeux vidéo. 

## Mes projets réalisés

* Création d'un jeu de poker en python


* Création d'un prototype de jeu de sim en JAVA


* Création du back-end d'un jeu de réhabilitation


* Création d'un tapis de roulement

### Project A

This is an image from an external site:

![This is the image caption](https://images.unsplash.com/photo-1512436991641-6745cdb1723f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ad25f4eb5444edddb0c5fb252a7f1dce&auto=format&fit=crop&w=900&q=80)

While this is an image from the assets/images folder. Never use absolute paths (starting with /) when linking local images, always relative.

![This is another caption](images/sample-photo.jpg)
